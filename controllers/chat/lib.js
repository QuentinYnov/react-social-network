const User = require('../../schema/User');
const Post = require('../../schema/Post');
const Conversation = require('../../schema/Conversation');

function createPost(req, res) {
    if (!req.body.content || !req.body.author || !req.body.conversation) {
        //Le cas où il manque des données
        res.status(400).json({
            "text": "Requête invalide"
        })
    } else {
        let post = {
            author: req.body.author,
            conversation: req.body.conversation,
            content: req.body.content
        };
        let findAuthor = new Promise(function (resolve, reject) {
            User.findOne({
                _id: post.author
            }, function (err, result) {
                if (err) { // erreur de co à la BDD
                    reject(500);
                } else {
                    if (result) {
                        // OK
                        resolve(true)
                    } else {
                        // User inconnu
                        reject(204)
                    }
                }
            })
        });

        findAuthor.then(function () {
            let _post = new Post(post);
            _post.save(function (err, user) {
                if (err) {
                    res.status(500).json({
                        "text": "Erreur interne"
                    })
                } else {
                    console.log(_post._id);
                    res.status(200).json({
                        "text": "Succès",
                    })
                }
            })
        }, function (error) {
            switch (error) {
                case 500:
                    res.status(500).json({
                        "text": "Erreur interne"
                    });
                    break;
                case 204:
                    res.status(204).json({
                        "text": "Utilisateur inconnu"
                    });
                    break;
                default:
                    res.status(500).json({
                        "text": "Erreur interne"
                    });
            }
        })
    }
}

function createConversation(req, res) {
    if (!req.body.title || !req.body.post || !req.body.users || req.body.users.length < 2) {
        //Le cas où il manque des données
        res.status(400).json({
            "text": "Requête invalide"
        })
    } else {
        let findAuthor = new Promise(function (resolve, reject) {
            User.findOne({
                _id: req.body.post.author
            }, function (err, result) {
                if (err) { // erreur de co à la BDD
                    reject(500);
                } else {
                    if (result) {
                        // user déja existant
                        resolve(true)
                    } else {
                        // User inconnu
                        reject(204)
                    }
                }
            })
        });

        findAuthor.then(function (author) {
            let conv = new Conversation({
                title: req.body.title,
                users: req.body.users
            });
            conv.save(function (err, _conv) {
                if (err) {
                    res.status(500).json({
                        "text": "Erreur enregistrement"
                    })
                } else {
                    let post = new Post(req.body.post);
                    post.conversation = _conv.id;
                    post.save(function (err, obj) {
                        if (err) {
                            res.status(500).json({
                                "text": "Erreur interne"
                            })
                        } else {
                            _conv.posts.push(obj._id);
                            _conv.save(function (err, obj) {
                                if (err) {
                                    res.status(500).json({text: "Erreur interne"})
                                }
                                res.status(200).json({
                                    "text": "Succès",
                                })
                            })
                        }
                    });
                }
            })
        }, function (error) {
            switch (error) {
                case 500:
                    res.status(500).json({
                        "text": "Erreur interne"
                    });
                    break;
                case 204:
                    res.status(204).json({
                        "text": "Utilisateur inconnu"
                    });
                    break;
                default:
                    res.status(500).json({
                        "text": "Erreur interne"
                    });
            }
        })
    }
}

function addPost(req, res) {
    if (!req.body.conversation || !req.body.post) {
        //Le cas où il manque des données
        res.status(400).json({
            "text": "Requête invalide"
        })
    } else {
        let findAuthor = new Promise(function (resolve, reject) {
            User.findOne({
                _id: req.body.post.author
            }, function (err, result) {
                if (err) { // erreur de co à la BDD
                    reject(500);
                } else {
                    if (result) {
                        // user déja existant
                        resolve(true)
                    } else {
                        // User inconnu
                        reject(204)
                    }
                }
            })
        });
        let findConv = new Promise(function (resolve, reject) {
            Conversation.findOne({
                _id: req.body.conversation
            }, function (err, result) {
                if (err) { // erreur de co à la BDD
                    reject(500);
                } else {
                    if (result) {
                        // user déja existant
                        resolve(result)
                    } else {
                        // Conversation inconnue
                        reject(204)
                    }
                }
            })
        });

        findAuthor.then(function () {
            findConv.then(function (conv) {
                let post = new Post(req.body.post);
                post.conversation = conv.id;
                post.save(function (err, obj) {
                    if (err) {
                        res.status(500).json({
                            "text": "Erreur enregistrement"
                        })
                    } else {
                        conv.posts.push(obj.id);
                        conv.save(function (err, obj) {
                            if (err) {
                                res.status(500).json({
                                    "text": "Erreur enregistrement"
                                })
                            } else {
                                res.status(200).json({
                                    "text": "Succès",
                                })
                            }
                        })
                    }
                })
            }, function (error) {
                res.status(500).json({
                    "text": "Conversation inconnue"
                })
            });
        }, function (error) {
            switch (error) {
                case 500:
                    res.status(500).json({
                        "text": "Erreur interne"
                    });
                    break;
                case 204:
                    res.status(204).json({
                        "text": "Utilisateur inconnu"
                    });
                    break;
                default:
                    res.status(500).json({
                        "text": "Erreur interne"
                    });
            }
        })
    }
}

function getUserConvs(req, res) {
    if (!req.body.user_id) {
        res.status(400).json({
            text: "Utilisateur inconnu"
        })
    } else {
        Conversation.find({
            users: {$all: req.body.user_id}
        }).populate('posts').exec(function (err, convs) {
            let _convs = [];
            convs.forEach(function (conv) {
                _convs.push(conv);
            });
            res.send(_convs);
        })
    }
}


exports.createPost = createPost;
exports.createConv = createConversation;
exports.userConvs = getUserConvs;
exports.addPost = addPost;