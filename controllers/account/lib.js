const PasswordReset = require("../../schema/PwdToken.js");
const uuidv1 = require('uuid/v1');
const config = require("../../config/config");
const User = require('../../schema/User.js');
const PasswordToken = require("../../schema/PwdToken");
const passwordHash = require("password-hash");


function signup(req, res) {
    if (!req.body.email || !req.body.password) {
        //Le cas où l'email ou bien le password serait null
        res.status(400).json({
            "text": "Requête invalide"
        })
    } else {
        let user = {
            email: req.body.email,
            password: passwordHash.generate(req.body.password),
            name: {
                displayname: req.body.displayname,
                firstname: req.body.firstname,
                lastname: req.body.lastname,
            },
            birthday: req.body.birthday,
            photo: req.body.photo
        };
        let findUser = new Promise(function (resolve, reject) {
            User.findOne({
                email: user.email
            }, function (err, result) {
                if (err) { // erreur de co à la BDD
                    reject(500);
                } else {
                    if (result) {
                        // user déja existant
                        reject(204)
                    } else {
                        // OK
                        resolve(true)
                    }
                }
            })
        });

        findUser.then(function () {
            let _u = new User(user);
            _u.token = _u.getToken().slice(0, 128);
            _u.save(function (err, user) {
                if (err) {
                    res.status(500).json({
                        "text": "Erreur interne"
                    })
                } else {
                    res.status(200).json({
                        "text": "Succès",
                        "token": user.token,
                        "uid": user._id
                    })
                }
            })
        }, function (error) {
            switch (error) {
                case 500:
                    res.status(500).json({
                        "text": "Erreur interne"
                    });
                    break;
                case 204:
                    res.status(204).json({
                        "text": "L'adresse email existe déjà"
                    });
                    break;
                default:
                    res.status(500).json({
                        "text": "Erreur interne"
                    });
            }
        })
    }
}

function login(req, res) {
    if (!req.body.email || !req.body.password) {
        //Le cas où l'email ou bien le password ne serait pas soumit ou nul
        res.status(400).json({
            "text": "Requête invalide"
        })
    } else {
        User.findOne({
            email: req.body.email
        }, function (err, user) {
            if (err) {
                res.status(500).json({
                    "text": "Erreur interne"
                })
            } else if (!user) {
                res.status(401).json({
                    "text": "L'utilisateur n'existe pas"
                })
            } else {
                if (user.authenticate(req.body.password)) {
                    user.token =  user.getToken().slice(0, 128);
                    user.save();
                    res.status(200).json({
                        "token": user.token,
                        "text": "Authentification réussi",
                    })
                } else {
                    res.status(401).json({
                        "text": "Mot de passe incorrect"
                    })
                }
            }
        })
    }
}

function logout(req, res) {
    res.status(200).json({
        text: "Déconnexion"
    })
}

function allUsers(req, res) {
    User.find({}, function (err, users) {
        let userMap = {};
        users.forEach(function (user) {
            userMap[user._id] = user;
        });
        res.send(userMap);
    });

}


function passwordReset(req, res) {
    if (!req.body.token) {
        res.status(400).json({
            "text": "Requête invalide"
        })
    } else {
        User.findOne({
            token: req.body.token
        }, function (err, user) {
            if (err) {
                res.status(500).json({
                    "text": "Erreur interne"
                })
            } else if (!user) {
                res.status(401).json({
                    "text": "L'utilisateur n'existe pas"
                })
            } else {
                if (user.email) {
                    let uuid = uuidv1();
                    let token = {
                        token: uuid,
                        user_id: user._id
                    };
                    let _token = new PasswordReset(token);
                    _token.save(function (err) {
                        if (err) {
                            console.log(err);
                            res.status(500).json({
                                "text": "Erreur interne"
                            })
                        }
                    });
                    let html = '<a href="http://localhost:3000/reset_pwd/' + uuid + '">You requested a password reset </a>';
                    let send = require('gmail-send')({
                        user: config.gmail_user,
                        pass: config.gmail_pwd,
                        to: user.email,
                        subject: 'Fullstack - Password Reset',
                        text: 'You requested a password reset !',
                        html: html,
                    }, function (err, res) {
                        res.status(err.status).json({
                            "text": res
                        });
                    });
                    send();
                    res.status(200).json({
                        "token": user.getToken(),
                        "text": "Authentification réussi",
                        "email": req.body.email
                    })
                } else {
                    res.status(401).json({
                        "text": "Email inconnu"
                    })
                }
            }
        })
    }
}

function handlePasswordReset(req, res) {
    if (!req.body.uuid || !req.body.password || !req.body.new_password || req.body.new_password !== req.body.cnew_password) {
        res.status(400).json({
            "text": "Requête invalide"
        })
    } else {
        PasswordToken.findOne({
            token: req.body.uuid
        }, function (err, token) {
            if (err) {
                res.status(500).json({
                    "text": "Erreur interne"
                })
            } else if (!token) {
                console.log("Unknown token");
                res.status(401).json({
                    "text": "Token invalide"
                })
            } else {
                //TODO: vérifier timestamp < 24h
                let created_at = new Date(token.created_at.toString());
                User.findOne({
                    _id: token.user_id
                }, function (err, user) {
                    if (err) {
                        res.status(500).json({
                            "text": "Erreur interne"
                        })
                    } else if (!user) {
                        res.status(401).json({
                            "text": "L'utilisateur n'existe pas"
                        })
                    }else{
                        if(user.authenticate(req.body.password)){
                            user.password = passwordHash.generate(req.body.new_password);
                            user.save();
                        }

                    }
                })

            }
        })
    }
}

function getUser(req, res){
    if(! req.body.token){
        res.status(500).json();
    }else{
        User.findOne({token:req.body.token}, function (err, user) {
            if(err){
                res.status(500).json();
            }else{
                res.send({user:user})
            }
        })
    }
}

exports.login = login;
exports.signup = signup;
exports.logout = logout;
exports.allUsers = allUsers;
exports.passwordReset = passwordReset;
exports.handlePasswordReset = handlePasswordReset;
exports.getUser = getUser;