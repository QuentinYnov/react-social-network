const account = require('./account/lib.js');
const chat = require("./chat/lib.js");


module.exports = function (app) {
    app.post('/user/login', account.login);
    app.post('/user/signup', account.signup);
    app.get('/user/logout', account.logout);
    app.get('/user/all', account.allUsers);
    app.post('/user/reset_pwd', account.passwordReset);
    app.post('/user/reset_password/', account.handlePasswordReset);
    app.get('/user/get/', account.getUser);

    app.post('/chat/post/new', chat.createPost);
    app.post('/chat/post/add', chat.addPost);
    app.post('/chat/conversation/new', chat.createConv);
    app.post('/chat/conversation/all', chat.userConvs);
};