const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require('body-parser');
const User = require('./schema/User');

mongoose.connect('mongodb://localhost/db').then(() => {
    console.log('Connected to mongoDB')
}).catch(e => {
    console.log('Error while DB connecting');
    console.log(e);
});


const app = express();

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

const http = require('http').createServer(app);
http.listen(8001, function () {
    console.log('listening on 8001');
});

const io = require('socket.io')(http);

io.on('connection', function (socket) {
    socket.on('chat_message', function (post) {
        User.findOne({_id: post.author}, function (err, user) {
            if (err || !user){
                console.log(err);
                console.log("no user");
            }
            console.log("Socket author: "+user.sockets);
            io.to(`${user.sockets}`).emit("chat_response", post);
        });
        User.findOne({_id: post.recipient}, function (err, user) {
            if (err || !user){
                console.log(err);
                console.log("no user");
            }
            console.log("Socket recipient: "+user.sockets);
            io.to(`${user.sockets}`).emit("chat_response", post);
        })
    });
    socket.on('chat_room', function (user) {
        User.findOneAndUpdate({_id: user}, {sockets: socket.id}, function (err, user) {
            console.log(socket.id);
        });
    });

});


let urlencodedParser = bodyParser.urlencoded({
    extended: true,
    limit: "10mb"
});

app.use(urlencodedParser);
app.use(bodyParser.json({limit: '10mb'}));


let router = express.Router();
app.use('/', router);
require(__dirname + '/controllers/AppController')(router);

let port = 8000;
app.listen(port, () => console.log(`Listening on port ${port}`));