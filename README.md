## Prérequis

- npm >= 6.4.1
- mongoDB Compass >= 1.17

## Installation

Pour récupérer le projet faire un git clone de ce [repo](https://gitlab.com/QuentinYnov/react-social-network).

Lancer la commande suivante à la racine du projet `npm install`.

Se rendre ensuite dans le dossier client et lancer à nouveau la commande `npm install`.


## Lancement du projet

1. Tout d'abord, démarrer le serveur mongoDB et le faire écouter le port 27017.
2. La base de données utilise le mode d'authentification "None" et le nom de base de données "db" doit être libre.
3. Exécuter la commande suivante à la racine du projet `node server.js`.
4. Ouvrir un nouveau terminal et exécuter la commande `npm start` dans le dossier `client`.


## Organisation du projet

- Le dossier `node_modules` contient les dépendances qui sont listées dans package.json

- Le dossier `controllers` contient les opérations backend et le routing de l'API.

- Le dossier `schema` contient les modèles de base données.

- Le dossier `config` contient les variables de configuration.