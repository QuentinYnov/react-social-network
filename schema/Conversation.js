const mongoose = require('mongoose');



let ConversationSchema = mongoose.Schema({
    title:{
        type:String,
        required:true
    },
    posts:[{type: mongoose.Schema.Types.ObjectId, ref: 'Post', required: true}],
    users: [{type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true}]
}, {timestamps: {createdAt: 'created_at'}});

module.exports = mongoose.model('Conversation', ConversationSchema);