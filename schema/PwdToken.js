const mongoose = require('mongoose');

let PwdTokenSchema = mongoose.Schema({
    token:{
        type:String,
        required:true,
    },
    user_id:{
        type:String,
        required:true
    }
}, {timestamps: {createdAt: 'created_at'}});

module.exports = mongoose.model('PasswordToken', PwdTokenSchema);