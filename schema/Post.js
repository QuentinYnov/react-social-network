const mongoose = require('mongoose');


let PostSchema = mongoose.Schema({
    conversation: {type: mongoose.Schema.Types.ObjectId, ref: 'Conversation', required: true},
    author: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
    content: {
        type: String,
        required: true
    }
}, {timestamps: {createdAt: 'created_at'}});

module.exports = mongoose.model('Post', PostSchema);