## Prérequis

- npm >= 6.4.1
- mongoDB Compass >= 1.17

## Installation

Se rendre dans le dossier client et lancer la commande `npm install`.


## Lancement du projet(front)

1. Exécuter les instructions de la section "Lancement du projet" situé dans le readme du dossier parent.
2. Exécuter la commande `npm start` et garder le terminal ouvert.

## Organisation du projet

- Le dossier `node_modules` contient les dépendances qui sont listées dans package.json

- Le dossier `public` contient le template html de base de l'application React et le manifeste de l'application.

- Le dossier `src` contient les fichiers utilisés par le front de l'application.

- Le dossier `components` regroupe les composants de l'application qui seront affichés.
- Le dossier `static` contient les fichiers statiques de l'application.
- Le dossier `utils` contient le fichier qui gère la connection entre le backend et le frontend.
