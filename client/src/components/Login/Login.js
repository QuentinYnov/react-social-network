import React from 'react';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import API from '../../utils/API';
import {Redirect} from "react-router";
import Navbar from "react-bootstrap/Navbar";
import NavbarBrand from "react-bootstrap/NavbarBrand";
import {Signup} from "../Signup/Signup";
import {ModalBox} from "../ModalBox/ModalBox"


export class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            login_failed: false,
        };
        this.handleChange.bind(this);
        this.send.bind(this);
        this.handleSubmit.bind(this);
    }

    send = event => {
        if (this.state.email.length === 0) {
            return;
        }
        if (this.state.password.length === 0) {
            return;
        }
        API.login(this.state.email, this.state.password)
            .then(function (data) {
                localStorage.setItem('token', data.data.token);
                window.location = "/dashboard"
            })
            .catch(err => {
                this.setState({"login_failed": true});
            })
    };
    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
        if (this.state.login_failed){
            this.setState({login_failed:false})
        }
    };
    handleSubmit = event => {
        event.preventDefault();
    };

    render() {
        if (API.isAuth()) {
            return <Redirect to='/dashboard'/>
        }
        return (
            <div>
                <Navbar className={"Login-header"}>
                    <NavbarBrand>
                        <img src={require("../../static/img/react.png")}
                             alt={"React"} className={"img-fluid"}/>
                    </NavbarBrand>
                    <div className={"mx-auto Login-title"}>
                        <NavbarBrand className={"text-white font-weight-bold m-auto"}>
                            React Social Network
                        </NavbarBrand>
                    </div>
                    <div className={"Login-buttons"}>
                        <Form className={"d-inline-flex"} onSubmit={this.handleSubmit}>
                            <Form.Control
                                placeholder="Email address"
                                type="email"
                                value={this.state.email}
                                onChange={this.handleChange}
                                id="email"
                                required
                            />
                            <Form.Control
                                placeholder="Password"
                                type="password"
                                value={this.state.password}
                                onChange={this.handleChange}
                                id="password"
                                required
                            />
                            <Button
                                disabled={!this.send}
                                type="submit"
                                onClick={this.send}
                                bssize={"small"}
                                className={"Login-button"}
                            >
                                Login
                            </Button>
                        </Form>
                        <ModalBox title={"Error"} show={this.state.login_failed} show_footer={false}
                                  text={"Login failed"} alert={"warning"} closeButton={false}
                        />
                    </div>
                </Navbar>
                <Signup mtop={true}/>
            </div>
        )
    }
}