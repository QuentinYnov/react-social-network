import React from 'react';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import API from '../../utils/API';
import Card from "react-bootstrap/Card";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import InputGroup from "react-bootstrap/InputGroup";
import Input from "reactstrap/es/Input";
import Label from "reactstrap/es/Label";
import Badge from "react-bootstrap/Badge";


//import {Datepicker} from "../Datepicker/Datepicker";

export class Signup extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            email: "",
            password: "",
            cpassword: "",
            firstname: "",
            lastname: "",
            displayname: "",
            birthday: "",
            picture: "",
            photo: "",

        };
        this.handleChange.bind(this);
        this.send.bind(this);
    }

    send = () => {
        if (this.state.email.length === 0) {
            return;
        }
        if (this.state.password.length === 0 || this.state.password !== this.state.cpassword) {
            return;
        }
        let _send = {
            email: this.state.email,
            password: this.state.password,
            firstname: this.state.firstname,
            lastname: this.state.lastname,
            displayname: this.state.displayname,
            birthday: this.state.birthday,
            photo: this.state.photo
        };
        API.signup(_send).then(function (res) {
            if (res.status === 204){
                alert("Cet email est déja utilisé pour un autre compte");
            }else{
                localStorage.setItem('token', res.data.token);
                window.location = "/"
            }
        }, function (error) {
            console.log(error);
        })
    };
    handleChange = event => {
        if (event.target.id === "picture") {
            if (event.target.files.length) {
                let reader = new FileReader();
                reader.readAsDataURL(event.target.files[0]);
                reader.onload = () => {
                    if (typeof reader.result === "string") {
                        this.setState({"photo": reader.result});
                    }
                };
                reader.onerror = function (error) {
                    console.log('Error: ', error);
                };
            }
        } else {
            this.setState({
                [event.target.id]: event.target.value
            });
        }
    };
    handleSubmit = event => {
        event.preventDefault();
    };

    render() {
        return (
            <Container className={this.props.mtop ? "clearfix SignUp-form" : "mt-4 SignUp-form"}>
                <Row>
                    <Col md={10} className={this.state.photo.length ? null : "offset-md-1"} id={"formCol"}>
                        <div>
                            <Card body>
                                <Form onSubmit={this.handleSubmit}>
                                    <div className={"text-left mb-2"}>
                                        <Badge variant={"success"} className={"mr-4"}>
                                            About you
                                        </Badge>
                                        <Badge>
                                            Enter your credentials
                                        </Badge>
                                    </div>
                                    <div className={"d-flex"}>
                                        <InputGroup className="mb-2 w-50">
                                            <InputGroup.Prepend>
                                                <InputGroup.Text className="font-weight-bold">
                                                    Firstname
                                                </InputGroup.Text>
                                            </InputGroup.Prepend>
                                            <Form.Control
                                                placeholder="Firstname"
                                                value={this.state.firstname}
                                                onChange={this.handleChange}
                                                id="firstname"
                                                required
                                            />
                                        </InputGroup>
                                        <InputGroup className="mb-2 w-50">
                                            <InputGroup.Prepend>
                                                <InputGroup.Text className="font-weight-bold">
                                                    Lastname
                                                </InputGroup.Text>
                                            </InputGroup.Prepend>
                                            <Form.Control
                                                placeholder="Lastname"
                                                value={this.state.lastname}
                                                onChange={this.handleChange}
                                                id="lastname"
                                                required
                                            />
                                        </InputGroup>
                                    </div>
                                    <InputGroup className="mb-2">
                                        <InputGroup.Prepend>
                                            <InputGroup.Text><FontAwesomeIcon icon="at"/></InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <Form.Control
                                            placeholder="Email address"
                                            type="email"
                                            value={this.state.email}
                                            onChange={this.handleChange}
                                            id="email"
                                            required
                                        />
                                    </InputGroup>
                                    <InputGroup className="mb-2">
                                        <InputGroup.Prepend>
                                            <InputGroup.Text>
                                                <FontAwesomeIcon icon="calendar-alt"/>
                                            </InputGroup.Text>
                                        </InputGroup.Prepend>
                                        {/*<Datepicker/>*/}
                                        <Form.Control
                                            placeholder="Birthday"
                                            type="date"
                                            value={this.state.birthday}
                                            onChange={this.handleChange}
                                            id="birthday"
                                            required
                                        />
                                    </InputGroup>
                                    <div className={"text-left my-2"}>
                                        <Badge variant={"success"} className={"mr-4"}>
                                            Social
                                        </Badge>
                                        <Badge>
                                            Create your profile
                                        </Badge>
                                    </div>
                                    <InputGroup className="mb-2 Signup-half">
                                        <InputGroup.Prepend>
                                            <InputGroup.Text><FontAwesomeIcon icon="user"/></InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <Form.Control
                                            placeholder="Display Name"
                                            value={this.state.displayname}
                                            onChange={this.handleChange}
                                            id="displayname"
                                            required
                                        />
                                    </InputGroup>
                                    <InputGroup className="mb-2 Signup-half">
                                        <InputGroup.Prepend>
                                            <InputGroup.Text>
                                                <FontAwesomeIcon icon="camera-retro"/>
                                            </InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <Label className={"btn btn-info form-control Signup-button"}>
                                            <Input
                                                value={this.state.picture}
                                                onChange={this.handleChange}
                                                id="picture"
                                                type="file"
                                                className={"d-none"}

                                            />
                                            <FontAwesomeIcon icon={"plus-circle"} className={"mr-2"}/>

                                            Add a profile picture
                                        </Label>
                                    </InputGroup>
                                    <div className={"text-left my-2"}>
                                        <Badge variant={"success"} className={"mr-4"}>
                                            Enter your password
                                        </Badge>
                                        <Badge>
                                            Your password but contains at least 8 characters, lower and upper case, a number and a special
                                            character.
                                        </Badge>
                                    </div>
                                    <InputGroup className="mb-2 Signup-half">
                                        <InputGroup.Prepend>
                                            <InputGroup.Text>
                                                <FontAwesomeIcon icon="key"/>
                                            </InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <Form.Control
                                            placeholder="Password"
                                            type="password"
                                            value={this.state.password}
                                            onChange={this.handleChange}
                                            id="password"
                                            required
                                        />
                                    </InputGroup>
                                    <InputGroup className="mb-2 Signup-half">
                                        <InputGroup.Prepend>
                                            <InputGroup.Text>
                                                <FontAwesomeIcon icon="key"/>
                                            </InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <Form.Control
                                            placeholder="Password confirmation"
                                            type="password"
                                            value={this.state.cpassword}
                                            onChange={this.handleChange}
                                            id="cpassword"
                                            required
                                        />
                                    </InputGroup>
                                    <Button
                                        onClick={this.send}
                                        bssize="large"
                                       // type="submit"
                                        //disabled={!this.send}
                                        className={"Signup-button"}
                                    >
                                        <FontAwesomeIcon icon={"sign-in-alt"} className={"mr-2 "}/>
                                        Signup
                                    </Button>
                                </Form>
                            </Card>
                        </div>
                    </Col>
                    <Col md={2}>
                        <div className={"h-25"}>
                        </div>
                        {this.state.photo.length ?
                            <div className={"text-center"}>
                                <img src={this.state.photo} id={"picturePreview"} className={"img-fluid rounded-circle"}
                                     alt={"Preview"}/>
                                <Badge className={"m-auto"} variant={"success"}>
                                    Preview of your profile picture
                                </Badge>
                            </div>
                            : null
                        }
                    </Col>
                </Row>
            </Container>
        )
    }
}