import React from "react";
import API from '../../utils/API';
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Badge from "react-bootstrap/Badge";
import socketIOClient from "socket.io-client";

const socket = socketIOClient('http://localhost:8001/');

export class Chatbox extends React.Component {
    constructor(props) {
        super(props);
        socket.emit("chat_room", this.props.author);
        this.state = {
            posts: this.props.posts,
            post: {
                content: "",
                author: this.props.author,
                recipient: this.props.recipient,
                created_at: ""
            },
            users: [this.props.users],
        };
        this.send.bind(this);
        this.handleChange.bind(this);

    };

    componentWillUnmount() {
        socket.off("chat_response");
    }

    send = event => {
        event.preventDefault();
        if (!this.props.author || !this.state.post.content.length) {
            return;
        }
        socket.emit("chat_message", this.state.post);
        API.createPost(this.state.post.content, this.props.author, this.props.recipient);
    };

    handleChange = event => {
        let post = {...this.state.post};
        post.content = event.target.value;
        post.created_at = new Date();
        this.setState({
            post: post,
        });
    };
    /**
     * Les fonctions suivantes assurent un scroll down sur la chatbox
     */
    scrollToBottom = () => {
        this.messagesEnd.scrollIntoView();
    };

    componentDidMount() {
        socket.on("chat_response", (post) => {
            console.log(post);
            if (this.state.posts.indexOf(post) === -1) {
                let posts = this.state.posts;
                if (!post.created_at) {
                    post.created_at = new Date();
                }
                posts.push(post);
                this.setState({
                    posts: posts,
                    post: {
                        content: "",
                        author: this.props.author,
                        recipient: this.props.recipient,
                        created_at: ""
                    },
                });
            }
        });
        this.scrollToBottom();
    }

    componentWillUpdate(nextProps, nextState, nextContext) {
        socket.emit("chat_room", this.props.author);
        if (this.state.posts.length !== nextProps.posts.length) {
            this.setState({posts: nextProps.posts});
        } else if (nextProps.posts.length > 0 && this.state.posts[0] !== nextProps.posts[0]) {
            this.setState({posts: nextProps.posts});
        }
        socket.on("chat_response", (post) => {
            if (this.state.posts.indexOf(post) === -1) {
                let posts = this.state.posts;
                if (!post.created_at) {
                    post.created_at = new Date();
                }
                posts.push(post);
                this.setState({
                    posts: posts,
                    post: {
                        content: "",
                        author: this.props.author,
                        recipient: this.props.recipient,
                        created_at: ""
                    },
                });
            }
        });
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    toggle = event => {
        let form = event.currentTarget.nextSibling;
        if (form.offsetParent === null) {
            form.classList.remove("d-none");
            this.scrollToBottom();
        } else {
            form.classList.add("d-none");
        }
    };

    render() {
        return (
            <div className={"d-flex flex-row-reserve"}>
                <div className={"chatbox-container w-25 fixed-bottom"}>
                    <div onClick={this.toggle} className={"bg-white chat-header py-2"}>
                        <b>{this.props.title}</b>
                    </div>
                    <div className={"chatbox bg-white"}>
                        <Form onSubmit={this.send}>
                            {this.state.posts.length ? this.state.posts.map((post, i) => {
                                let msg_class = "my-1 rounded p-2 msg " + (this.props.author === post.author ? "msg-author float-left" : "msg-recipient bg-info float-right");
                                return (
                                    <div className={msg_class} key={"chatbox-" + i}>
                                        {post.content}
                                        <br/>
                                        <Badge variant={"light"} className={"m-auto"}>
                                            {post.created_at && new Date(post.created_at).toLocaleDateString() + " " + new Date(post.created_at).toLocaleTimeString()}
                                        </Badge>
                                    </div>
                                )
                            }) : "Chat with " + this.props.title}
                            <div style={{float: "left", clear: "both"}}
                                 ref={(el) => {
                                     this.messagesEnd = el;
                                 }}>
                            </div>
                            <Form.Control
                                value={this.state.post.content}
                                id={"content"}
                                onChange={this.handleChange}
                                autoComplete={"off"}
                            />
                            <Button type={"submit"} bssize={"small"}>Submit</Button>
                        </Form>
                    </div>
                </div>
            </div>
        )
    }
}