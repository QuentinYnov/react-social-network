import React from "react";
//import Input from "reactstrap/es/Input";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

export class Datepicker extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            day: "",
            month: "January",
            year: "",
            date: ""
        };
        this.months = ["January", "February", "Mars", "April", "May", "June", "July", "August",
            "September", "October", "November", "December"];
    }

    createDays() {
        let max = 31,
            days = [];
        if (this.months.indexOf(this.state.month) % 2) {
            max = 30;
        }
        for (let i = 1; i <= max; i++) {
            days.push(<option value={i}>{i}</option>)
        }
        return days
    }

    render() {
        let i = 0;
        return (
            <Row>
                <Col>
                    <select name={"month"} className={"form-control mr-2 px-4"}>
                        {this.months.map((month) => {
                            i++;
                            return <option value={i}>{month}</option>
                        })}
                    </select>
                </Col>
                <Col>
                    <select name={"day"} className={"form-control"}>
                        {this.createDays()}
                    </select>
                </Col>
            </Row>
        )

    }
}