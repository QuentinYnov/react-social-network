import React from 'react';
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import API from "../../utils/API";
import Card from "react-bootstrap/Card";
import {Chatbox} from "../Chat/Chatbox";
import Figure from "react-bootstrap/Figure";

const default_pic = require("../../static/img/react.png");

export class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            user: null,
            conversation: null,
        };
    }

    componentWillMount() {
        API.getUser(localStorage.getItem("token")).then(res => {
            this.setState({user: res.data.user});
        });
        API.allUsers().then(data => {
            let users = [];
            Object.keys(data.data).map(key => {
                if (data.data[key].token !== localStorage.getItem("token")) {
                    users.push(data.data[key]);
                }
                return true;
            });
            this.setState({users: users});
        }).catch(err => {
            console.log(err);
        });
    }

    showConversation = event => {
        let user_id = event.currentTarget.dataset.userId,
            title = event.currentTarget.textContent;
        API.getPosts(this.state.user._id, user_id).then(res => {
            let posts = [];
            for (let post of res.data) {
                posts.push(post)
            }
            let conv = <Chatbox author={this.state.user._id}
                                recipient={user_id}
                                title={title}
                                posts={posts}
            />;

            this.setState({conversation: conv});
        }).catch(err => console.log(err));

    };

    render() {
        const {users} = this.state;
        return (
            <div>
                <Container className={"mt-4"}>
                    <Row>
                        <Col sm={1} md={1} lg={1} className={"d-block"}>
                            {
                                users && users.length && users.map((user, i) =>
                                    (<div data-user-id={user._id} onClick={this.showConversation} key={"col-" + i}
                                          className={"figure-chat"}>
                                            <Figure>
                                                <Figure.Image
                                                    width={64}
                                                    height={64}
                                                    src={user.photo ? user.photo : default_pic}
                                                    roundedCircle={true}
                                                    fluid={true}
                                                    className={"bg-white p-2"}
                                                />
                                                <Figure.Caption>
                                                    {user.name.displayname}
                                                </Figure.Caption>
                                            </Figure>
                                        </div>
                                    )
                                )
                            }
                        </Col>
                        <Col sm={8} md={8} lg={8} className={"mb-2"}>
                            {
                                users && users.length && users.map((user, i) =>
                                    (
                                        <Card body key={"card-" + i}>

                                            <img src={user.photo ? user.photo : default_pic} alt={"Profile"}
                                                 height={"100"}
                                            />
                                            <p className={"my-2"}> {user.name.displayname} </p>
                                        </Card>
                                    )
                                )
                            }
                        </Col>
                        <Col sm={3} md={3} lg={3}>
                            {this.state.conversation && this.state.conversation}
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}