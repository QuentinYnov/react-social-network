import React from 'react';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import API from '../../utils/API';
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import InputGroup from "react-bootstrap/InputGroup";



export class PasswordReset extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            password: "",
            new_password: "",
            cnew_password: "",
            uuid:this.props.match.params.token
        };
        this.handleChange.bind(this);
        this.send.bind(this);
        this.handleSubmit.bind(this);
    }

    send = event => {
        if (this.state.password === 0) {
            return;
        }
        if (this.state.new_password.length === 0 || this.state.new_password !== this.state.cnew_password) {
            return;
        }
        let _send = {
            password: this.state.password,
            new_password: this.state.new_password,
            cnew_password: this.state.cnew_password,
            uuid:this.state.uuid
        };
        API.handlePasswordReset(_send).then(function (data) {
            API.logout();
            window.location = "/";
        }, function (error) {
            console.log(error);
        })
    };

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    };
    handleSubmit = event => {
        event.preventDefault();
    };

    render() {
        return (
            <Container className="mt-4">
                <Row>
                    <Col className="col-md-10 offset-md-1">
                        <h3 className={"text-primary font-weight-bold mt-2 mb-4"}>
                            Reset your password
                        </h3>
                        <Form onSubmit={this.handleSubmit}>
                            <InputGroup className="mb-3">
                                <InputGroup.Prepend>
                                    <InputGroup.Text>
                                        <FontAwesomeIcon icon="key"/>
                                    </InputGroup.Text>
                                </InputGroup.Prepend>
                                <Form.Control
                                    placeholder="Current password"
                                    type="password"
                                    value={this.state.password}
                                    onChange={this.handleChange}
                                    id="password"
                                    required
                                />
                            </InputGroup>
                            <InputGroup className="mb-3">
                                <InputGroup.Prepend>
                                    <InputGroup.Text>
                                        <FontAwesomeIcon icon="key"/>
                                    </InputGroup.Text>
                                </InputGroup.Prepend>
                                <Form.Control
                                    placeholder="New password"
                                    type="password"
                                    value={this.state.cpassword}
                                    onChange={this.handleChange}
                                    id="new_password"
                                    required
                                />
                            </InputGroup>
                            <InputGroup className="mb-3">
                                <InputGroup.Prepend>
                                    <InputGroup.Text>
                                        <FontAwesomeIcon icon="key"/>
                                    </InputGroup.Text>
                                </InputGroup.Prepend>
                                <Form.Control
                                    placeholder="Confirm your new password"
                                    type="password"
                                    value={this.state.cpassword}
                                    onChange={this.handleChange}
                                    id="cnew_password"
                                    required
                                />
                            </InputGroup>
                            <Button
                                onClick={this.send}
                                bssize="large"
                                type="submit">
                                Reset Password
                            </Button>
                        </Form>
                    </Col>
                </Row>
            </Container>
        )
    }
}