import React from "react";
import API from "../../utils/API";
import Dropdown from "react-bootstrap/Dropdown";

export class PasswordResetLink extends React.Component {
    constructor(props) {
        super(props);
        this.passwordReset.bind(this);
    }

    passwordReset() {
        API.passwordReset();
    }

    render() {
        return <Dropdown.Item onClick={this.passwordReset}>Reset Password</Dropdown.Item>
    }
}