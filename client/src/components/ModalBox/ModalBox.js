import React from "react";
import Modal from "react-bootstrap/Modal";
import Alert from "react-bootstrap/Alert";

export class ModalBox extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);

        this.state = {
            show: this.props.show,
        };
    }

    handleClose() {
        this.setState({show: false});
    }

    handleShow() {
        this.setState({show: true});
    }

    componentWillUpdate(nextProps, nextState, nextContext) {
        if (this.state.show !== nextProps.show)
            this.setState({show:nextProps.show});
    }

    render() {
        return (
            <>
                <Modal show={this.state.show} onHide={this.handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>{this.props.title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {this.props.alert ?
                            <Alert variant={this.props.alert}>
                                {this.props.text}
                            </Alert>
                            : this.props.text
                        }

                    </Modal.Body>
                </Modal>
            </>
        );
    }
}