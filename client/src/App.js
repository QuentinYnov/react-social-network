import React, {Component} from 'react';
import {Route, Switch} from 'react-router-dom';
import {Dashboard} from './components/Dashboard/Dashboard.js';
import {Login} from './components/Login/Login.js';
import {Signup} from './components/Signup/Signup.js';
import {PrivateRoute} from './components/PrivateRoute.js';
import {library} from '@fortawesome/fontawesome-svg-core'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faAt, faCameraRetro, faPlusCircle, faSignInAlt, faUserCircle} from "@fortawesome/free-solid-svg-icons";
import {faKey} from "@fortawesome/free-solid-svg-icons";
import {faCalendarAlt} from "@fortawesome/free-solid-svg-icons/faCalendarAlt";
import './App.css';
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import {faUser} from "@fortawesome/free-solid-svg-icons/faUser";
import API from "./utils/API";
import Dropdown from "react-bootstrap/Dropdown";
import {PasswordReset} from "./components/PasswordReset/PasswordReset";
import {PasswordResetLink} from "./components/Utils/PasswordResetLink";


library.add(faAt);
library.add(faKey);
library.add(faCalendarAlt);
library.add(faUser);
library.add(faUserCircle);
library.add(faCameraRetro);
library.add(faSignInAlt);
library.add(faPlusCircle);


class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: null
        };
        this.disconnect.bind(this);
    }

    disconnect = event => {
        API.logout();
        window.location = "/";
    };

    componentWillMount() {
        if (API.isAuth()) {
            API.getUser(localStorage.getItem("token")).then(res => {
                if (res.data.user) {
                    this.setState({user: res.data.user.name.displayname})
                }
            });
        }
    }

    render() {
        if (API.isAuth()) {
            return (
                <div className="App">
                    <div className="App-content">
                        <Navbar className={"Navbar-dashboard"}>
                            <Navbar.Brand href="/">React Social Network</Navbar.Brand>
                            <Nav className="mr-auto">
                                <Nav.Link href={"/"}>{this.state.user && this.state.user}</Nav.Link>
                            </Nav>
                            <Dropdown>
                                <Dropdown.Toggle variant={"secondary"} id="dropdown-basic">
                                    <FontAwesomeIcon icon="user-circle"/>
                                </Dropdown.Toggle>
                                <Dropdown.Menu className={"bg-secondary menu-dashboard"}>
                                    <PasswordResetLink/>
                                    <Dropdown.Item onClick={this.disconnect}>Logout</Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                        </Navbar>
                        <Switch>
                            <Route exact path="/" component={Login}/>
                            <Route exact path="/login" component={Login}/>
                            <Route exact path="/logout"/>
                            <Route
                                path="/reset_pwd/:token([0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12})"
                                component={PasswordReset}/>
                            <Route exact path="/signup" component={Signup}/>
                            <PrivateRoute path='/dashboard' component={Dashboard}/>
                        </Switch>
                    </div>
                </div>
            );
        } else {
            return (
                <div className="App">
                    <div className="App-content">
                        <Switch>
                            <Route exact path="/" component={Login}/>
                            <Route exact path="/login" component={Login}/>
                            <Route exact path="/signup" component={Signup}/>
                            <PrivateRoute path='/dashboard' component={Dashboard}/>
                        </Switch>
                    </div>
                </div>
            );
        }
    }
}

export default App;