import axios from 'axios';

const headers = {
    'Content-Type': 'application/json'
};
const burl = "http://localhost:8000";

export default {
    login: function (email, password) {
        return axios.post(burl + '/user/login', {
            'email': email,
            'password': password
        }, {
            headers: headers
        })
    },
    signup: function (send) {
        return axios.post(burl + '/user/signup', send, {headers: headers})
    },

    isAuth: function () {
        return (localStorage.getItem('token') !== null);
    },
    logout: function () {
        localStorage.clear();
    },

    allUsers: function (req) {
        return axios.get(burl + "/user/all", {headers: headers})
    },
    passwordReset: function () {
        return axios.post(
            burl + "/user/reset_pwd",
            {token: localStorage.getItem('token')},
            {headers: headers}
        )
    },
    handlePasswordReset: function (send) {
        return axios.post(
            burl + "/user/reset_password",
            send,
            {headers: headers}
        )
    },
    createPost: function (content, author, conv) {
        return axios.post(
            burl + "/chat/post/new",
            {
                content: content,
                author: author,
                conversation: conv
            }, {headers: headers}
        )
    },
    addPost: function (conv, post) {
        return axios.post(burl + "/chat/post/add",
            {
                conversation: conv,
                post: post
            }, {headers: headers}
        )
    },
    createConv: function (title, post, users) {
        return axios.post(
            burl + "/chat/conversation/new",
            {
                title: title,
                post: post,
                users:users
            }, {headers: headers}
        )
    },
    getUsersConvs: function (user_id) {
        return axios.post(burl + "/chat/conversation/all",
            {
                user_id: user_id
            }, {headers: headers})
    },
    getUser:function (token) {
        return axios.get(burl+"/user/get/", {headers:headers, params:{token:token}})
    }
}

